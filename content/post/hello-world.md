---
title: "Hello World"
date: 2022-04-03T09:24:44-05:00
aplayer: true
---

Testing some stuff in Hugo.

<!--more-->

## LaTeX

{{< math >}}
x = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a}
{{< /math >}}

You can write a squareroot as {{<math "inline">}} \sqrt{x} {{</math>}}.

## YouTube

{{< youtube dQw4w9WgXcQ >}}

## Image

![Dutch Boy](/img/dutch-boy.png)

## Emoji

When the site is statically generated!:flushed:

## Code

Hello World in Python is simply `print("hello world")`.

The fast inverse squareroot algorithm from *Quake III Arena*, written in C, with the
original comments.

```
float Q_rsqrt( float number )
{
	long i;
	float x2, y;
	const float threehalfs = 1.5F;

	x2 = number * 0.5F;
	y  = number;
	i  = * ( long * ) &y;                       // evil floating point bit level hacking
	i  = 0x5f3759df - ( i >> 1 );               // what the fuck? 
	y  = * ( float * ) &i;
	y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
//	y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed

	return y;
}
```

## APlayer

For whatever reason, spaces don't seem to register and the player displays it all as one word.
Probably from the theme, I'll use periods for spaces for now.

{{< aplayer urls="/audio/otis-mcmusic.mp3" names="Otis.McMusic" artists="Otis.McDonald" covers="/img/innovations-in-booty-shaking-music-vol-1.jpg" >}}
